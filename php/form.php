
<meta http-equiv='cache-control' content='no-cache'> 
<meta http-equiv='expires' content='0'> 
<meta http-equiv='pragma' content='no-cache'> 
<?php
    $name = '';
    $password = '';
    $gender = '';
    $color = '';
    $languages = [];
    $comments = '';
    $tc = '';

    if (isset($_POST['submit'])) {
       // echo htmlspecialchars($_POST['searchterm'],ENT_QUOTES);
       if (isset($_POST['name'])) {
           $name = $_POST['name'];
           if ($name!='') {
            printf('User name: %s',htmlspecialchars($name,ENT_QUOTES));
           }
       };
       if (isset($_POST['password'])) {
           $password = $_POST['password'];
           if ($password!='') {
            printf('<br>Password: %s',htmlspecialchars($password,ENT_QUOTES));
           }
       };
       if (isset($_POST['gender'])) {
           $gender = $_POST['gender'];
           if ($gender!='') {
            printf('<br>Gender: %s',htmlspecialchars($gender,ENT_QUOTES));
           }
       };
       if (isset($_POST['color'])) {
           $color = $_POST['color'];
           if ($color!='') {
            printf('<br>Color: %s',htmlspecialchars($color,ENT_QUOTES));
           }
       };
       if (isset($_POST['languages'])) {
           $languages = $_POST['languages'];
           if ($languages!='') {
            printf('<br>Language(s): %s',htmlspecialchars(implode(' ',$languages),ENT_QUOTES));
           }
       };
       if (isset($_POST['comments'])) {
           $comments = $_POST['comments'];
           if ($comments!='') {
            printf('<br>Comments: %s',htmlspecialchars($comments,ENT_QUOTES));
           }
       };
       if (isset($_POST['tc'])) {
           $tc = $_POST['tc'];
           if ($tc!='') {
            printf('<br>T&amp;C: %s',htmlspecialchars($tc,ENT_QUOTES));
           }
       };       
    //    printf('User name: %s
    //         <br>Password: %s
    //         <br>Gender: %s
    //         <br>Language(s): %s
    //         <br>Comments: %s
    //         <br>T&amp;C: %s',
    //         htmlspecialchars($name,ENT_QUOTES),
    //         htmlspecialchars($password,ENT_QUOTES),
    //         htmlspecialchars($gender,ENT_QUOTES),
    //         htmlspecialchars(implode(' ',$languages),ENT_QUOTES),
    //         htmlspecialchars($comments,ENT_QUOTES),
    //         htmlspecialchars($tc,ENT_QUOTES));
    }
?> <br>
<form
    action=""
    method="post">
    User name: <input type="text" name="name"><br>
    Password: <input type="password" name="password"><br>
    Gender: 
        <input type="radio" name="gender" value="f"> female
        <input type="radio" name="gender" value="m"> male
        <input type="radio" name="gender" value="o"> other <br />
    Favorite color:
        <select name="color">
            <option value="">Please select</option>
            <option value="#f00">Red</option>
            <option value="#0f0">Green</option>
            <option value="#00f">Blue</option>
        </select>  <br>
    Languages Spoken:
    <select name="languages[]" multiple size="3">
            <option value="en">English</option>
            <option value="fr">French</option>
            <option value="it">Italien</option>
        </select>  <br> 
    Comments: <textarea name="comments"></textarea><br> 
    <input type="checkbox" name="tc" value="ok">
        I accept the terms &amp; conditions <br>
    <input type="submit" name="submit" value="Register">
</form>