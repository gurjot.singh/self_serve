<?php
    $_SERVER["REMOTE_ADDR"] = "http://localhost:8080";
    date_default_timezone_set('America/New_York');
    require '../vendor/autoload.php';
    use Elasticsearch\ClientBuilder;
    $scheme = 'https';
    $hostname = 'elasticsearch-datastealth.apps.ocp-dev-ade.belldev.dev.bce.ca';
    $port = '443';
    // $username = 'logstash_internal';
    // $password = 'logstash_internal';
    $username = 'php_internal';
    $password = 'php_internal';

    $hosts = [
        [
            'host' => $hostname,
            'port' => $port,
            'scheme' => $scheme,
            'user' => $username,
            'pass' => $password
        ],
    ];
    
    // $myCert = '/etc/certs/BellIssuingCA4.cer';
    $client = ClientBuilder::create()           // Instantiate a new ClientBuilder
                        // ->setConnectionPool('\Elasticsearch\ConnectionPool\StaticNoPingConnectionPool', [])
                        ->setHosts($hosts)      // Set the hosts
                        // ->setSSLVerification($myCert)
                        ->setSSLVerification(false)
                        ->build();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- <title>ElasticSearch PDF Report Generation</title> -->
    <link rel="stylesheet" href="../styles/styles.css">
    <script src="../javascript/init.js"></script> 

    <title>ElasticSearch PDF Report Generation</title>

    <div class="header" style="line-height: 0.3">
        <h1>
            <!-- <img style="vertical-align:top;margin:-5px 0px" src="/images/Datex.png" alt="Datex" width=40 height=40></img><img src="/images/Bell.png" alt="Bell" width=90 height=50></img>  -->
            ElasticSearch PDF Report Generation
        </h1>
        <!-- <hr> -->
    </div>

</head>
<body>
    <!-- <h1>ElasticSearch PDF Report Generation:</h1><br> -->
        <svg class="svg" height="20" width="100%">
                <path  class="svg-path" onclick="openMenu()" d="M0 2h16v2H0V2zm0 5h16v2H0V7zm16 5H0v2h16v-2z" >
        </svg>

    <form id="pdf_form"
        action=""
        method="post">
        <br>
        <text style="padding-left: 20px;"><input required placeholder="Enter Kibana POST URL" style="height: 20px;width: 1000px;background-color: #ebeceebb;" type="text" name="url">
        <span class="validity"></span><br></text>
        <br>
        <text style="padding-left: 20px;color:#a81106;font-style: italic;">***PDF Report generation can take 2-3 minutes. By default, report generation times out if the report cannot be generated within two minutes. If you are generating reports that contain many complex visualizations or your machine is slow or under constant heavy load, it might take longer than two minutes to generate a report***</text><br><br>
        <div style="padding-left: 20px;"><input type="submit" id="submit" name="submit" value="Generate PDF"> <br> <br></div>
        <!-- <button onClick="window.print()">Print this page</button><br> <br> -->
    </form>
    <p>
        <?php
            # setup a global file pointer
            if (isset($_POST['submit'])) {

                // $jobParams = "%28browserTimezone%3AAmerica%2FNew_York%2Clayout%3A%28dimensions%3A%28height%3A2864%2Cwidth%3A1274%29%2Cid%3Apreserve_layout%29%2CobjectType%3Adashboard%2CrelativeUrls%3A%21%28%27%2Fapp%2Fdashboards%23%2Fview%2F4a6532e0-2ab7-11eb-870b-63703208df3c%3F_g%3D%28filters%3A%21%21%28%29%2CrefreshInterval%3A%28pause%3A%21%21t%2Cvalue%3A0%29%2Ctime%3A%28from%3Anow-30d%2Cto%3Anow%29%29%26_a%3D%28description%3A%21%27%21%27%2Cfilters%3A%21%21%28%29%2CfullScreenMode%3A%21%21f%2Coptions%3A%28hidePanelTitles%3A%21%21f%2CuseMargins%3A%21%21t%29%2Cquery%3A%28language%3Akuery%2Cquery%3A%21%27%21%27%29%2CtimeRestore%3A%21%21t%2Ctitle%3A%21%27d.ds.database_discovery_dashboard%2520-%2520Generic%21%27%2CviewMode%3Aview%29%27%29%2Ctitle%3A%27d.ds.database_discovery_dashboard%20-%20Generic%27%29";
                // $url = "https://kibana-datastealth.apps.ocp-dev-ade.belldev.dev.bce.ca/api/reporting/generate/printablePdf?jobParams=$jobParams";
                if (isset($_POST['url'])) {
                    $url = $_POST['url'];
                    if ($url!='' AND strpos($url, "kibana-datastealth.apps.ocp-dev-ade.belldev.dev.bce.ca") !== false) {
                        $ch = curl_init();
                        // echo "URL found";
                        curl_setopt($ch, CURLOPT_URL, $url);
                        curl_setopt($ch, CURLOPT_POST, 1);
                        curl_setopt($ch, CURLOPT_USERPWD, $username . ':' . $password);
                        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        
                        $headers = [
                            'kbn-xsrf: reporting',
                            'kbn-version: 7.9.1',
                            'Accept:application/json; charset=utf-8',
                            'Content-type:application/json; charset=utf-8'
                        ];
        
                        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

                        try {
                            $output = curl_exec($ch);
                            $result =json_encode($output, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
                            $data = json_decode($output);
                            $res_url = "https://kibana-datastealth.apps.ocp-dev-ade.belldev.dev.bce.ca".$data->path;
                            // print_r("<hr><b>Download Path:</b> <pre>$res_url</pre>"); 
                            curl_close ($ch); 
                            print_r("<hr><b>POST Result:</b> <pre>$result</pre>"); 
                            // print_r("<pre>".$result."</pre><hr>");
                        } catch (Exception $e) {
                            curl_close ($ch); 
                            echo 'Caught exception: ',  $e->getMessage(), "\n";
                        };
        
                        // sleep(30);
                        // echo "Sleep over";
                        $ch = curl_init();
                        
                        curl_setopt($ch, CURLOPT_URL, $res_url);
                        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET"); 
                        curl_setopt($ch, CURLOPT_FILE, $GlobalFileHandle);
                        curl_setopt($ch, CURLOPT_USERPWD, $username . ':' . $password);
                        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        
                        $body = '{}';
                        // $attachment_location = "../downloads/test.pdf";
                        $headers = [
                            'kbn-version: 7.9.1',
                            'Accept:application/pdf',
                            'Content-type: application/pdf',
                            // 'Content-Transfer-Encoding: Binary',
                            'Content-Type: application/force-download'
                            // 'Content-Length:'.filesize($attachment_location),
                            // 'Content-Disposition: inline; filename='.$attachment_location
                        ];
        
                        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                        curl_setopt($ch, CURLOPT_HEADER, 0);
                        curl_setopt($ch, CURLOPT_POSTFIELDS,$body);

                        try {
                            $key = true;
                            $starttime = new DateTime(date('H:i:s'));
                            // printf('<script language="javascript">show_pdf_load();</script>');
                            while ($key) {
                                $res = curl_exec($ch);  
                                if (preg_match("/PDF/",$res)) {
                                    $key = false;
                                    $endtime = new DateTime(date('H:i:s'));
                                    // printf('<script language="javascript">hide_pdf_load();</script>');
                                    break;
                                } 
                            }
                            $diffe = $endtime->diff( $starttime );
                            
                            print_r("<b>Elapsed Time:</b> <pre>"); 
                            echo $diffe->format( '%H:%I:%S' );
                            print_r("</pre><br>"); 

                            $filesize = curl_getinfo($ch, CURLINFO_CONTENT_LENGTH_DOWNLOAD);
                            if (curl_errno($ch)) {
                                echo 'Error:' . curl_error($ch);
                            }
                            curl_close($ch);
                            // print_r("Filesize: <pre>".$filesize."</pre><hr>"); 
                            // Inject $res into a file //
                            // POP up the file for user to download //
                            
                            $filename= 'datastealth_report_'.date('Y-m-d_H_i_s').'.pdf';
                            $directory="downloads";
                            $file=fopen("../$directory/$filename", "w") or die("Unable to open file!");
                            $urlfile=$_SERVER["REMOTE_ADDR"]."/$directory/$filename";
                            // echo $urlfile;
                            fwrite($file, $res);
                            // file_put_contents($file, $res);
                            fclose($file);
                            print_r("<b>Downloaded Report:</b> <br><br>"); 
                            print_r("<iframe src='$urlfile' frameBorder='0' scrolling='auto' height='900px' width='100%'></iframe>");
                            
                            // unlink("../$directory/$filename");
                            // print_r("Result: <br><pre>".$res."</pre><hr>");  

                        } catch (Exception $e) {
                            curl_close($ch);
                            echo 'Caught exception: ',  $e->getMessage(), "\n";
                        };
                    }
                    else {
                        print_r("<hr><b style='color:#a81106;'>Invalid URL</b>"); 
                    }
                };          
            };  

        ?>
    </p>
</body>
</html>
