<?php
    session_start(); //Start the session
    $name = '';
    $password = '';
    $tc = '';
    // $host = '';
    $user_name='demo';
    $pass_word='root';
    $_SESSION['loggedIn'] = false;

    if (isset($_POST['submit'])) {
        // echo htmlspecialchars($_POST['searchterm'],ENT_QUOTES);
         if (isset($_POST['name']) and isset($_POST['password']) and isset($_POST['tc'])) {

            // LDAP Integration
            // $ladServer = "LDAPS://ldaps.bell.corp.bce.ca:636";

            // $ldap = ldap_connect($ladServer);
            // $username = $_POST['name'];
            // $password = $_POST['password'];

            // $domain_name = 'BELL.CORP.BCE.CA';

            // $ldaprdn = $domain_name . "\\" . $username;

            // ldap_set_option($ldap, LDAP_OPT_PROTOCOL_VERSION, 3);
            // ldap_set_option($ldap, LDAP_OPT_REFERRALS, 0);

            // $bind = @ldap_bind($ldap, $ldaprdn, $password);

            // if ($bind) {
            //     $filter="(sAMAccountName=$username)";
            //     $result = ldap_search($ldap,"CN=datastealth_elk-admin-uat-access,OU=Bell Groups,OU=Bell,OU=Business Units,DC=bell,DC=corp,DC=bce,DC=ca",$filter);
            //     ldap_sort($ldap,$result,"sn");
            //     $info = ldap_get_entries($ldap, $result);
            //     for ($i=0; $i<$info["count"]; $i++)
            //     {
            //         if($info['count'] > 1)
            //             break;
            //         echo "<p>You are accessing <strong> ". $info[$i]["sn"][0] .", " . $info[$i]["givenname"][0] ."</strong><br /> (" . $info[$i]["samaccountname"][0] .")</p>\n";
            //         echo '<pre>';
            //         var_dump($info);
            //         echo '</pre>';
            //         $userDn = $info[$i]["distinguishedname"][0]; 
            //     }
            //     @ldap_close($ldap);
            // } else {
            //     $msg = "Invalid email address / password";
            //     echo $msg;
            // }

            // LDAP Integration

            $name = $_POST['name'];
            $password = $_POST['password'];
            $tc = $_POST['tc'];
            if ($name==$user_name and $password==$pass_word and $tc=='ok') {
                $_SESSION['loggedIn'] = true;
                // printf('<script language="javascript">redirect("report.php");</script>');     
                header("Location: /php/report.php");         
            }
            else {
                $_SESSION['loggedIn'] = false;
            }
         };
        //  else {
        //     printf('<script language="javascript">unhide();</script>');
        //  };
    }  
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        <link rel="stylesheet" href="/styles/styles.css">
        <script src="/javascript/init.js"></script> 

        <title>Self Serve - Login Page</title>

        <div class="header" style="line-height: 0.3">
            <h1>
                <!-- <img style="vertical-align:top;margin:-5px 0px" src="/images/Datex.png" alt="Datex" width=40 height=40></img> -->
                <img src="/images/Bell.png" alt="Bell" width=90 height=50></img> 
                Self Serve Portal
            </h1>
            <!-- <hr> -->
        </div>
        
    </head>
    <body>
         <div class="divsvg" height="20" width="100%">
            <button style="border: none;background: none;padding-top: 6px;">
                <svg height="20" width="20">
                        <path  class="svg-path" onclick="openMenu()" d="M0 2h16v2H0V2zm0 5h16v2H0V7zm16 5H0v2h16v-2z" >
                </svg>
            </button>
        </div>
        <form id="login_form"
            action=""
            method="post">
            <div id="div_alert" class="alert" style="display: none;"><span id="alert" class="closebtn" onClick="javascript:this.parentElement.style.display='none';">&times;</span> 
                  <strong>!</strong> Please enter the correct credentials and Accept the terms &amp; conditions.
            </div>
            <br>
            <text style="padding-left: 20px;">User name: <input type="text" placeholder="Enter your username" name="name" style="height: 20px;width: 200px;background-color: #ebeceebb;" required><br><br>
            <text style="padding-left: 20px;">Password: <input type="password" placeholder="Enter your pasword" name="password" style="height: 20px;width: 200px;background-color: #ebeceebb;" required><br><br>
            <text style="padding-left: 20px;"><input type="checkbox" name="tc" value="ok">
                I Accept the terms &amp; conditions <br><br>
            <text style="padding-left: 20px;"><input type="submit" id="submit" name="submit" value="Login"><br> <br>
            <!-- <button onClick="window.print()">Print this page</button><br> <br> -->
        </form>
        <p>
            <?php
                if (isset($_POST['submit'])) {
                    if (!isset($_POST['name']) or !isset($_POST['password']) or !isset($_POST['tc'])) {
                        printf('<script language="javascript">unhide();</script>');
                        $_SESSION['loggedIn']=false;
                    }
                    else if ($_POST['name']!=$user_name or $_POST['password']!=$pass_word or $_POST['tc']!='ok') {
                        printf('<script language="javascript">unhide();</script>');
                        $_SESSION['loggedIn']=false;
                    };
                }  
            ?>
        </p>
        <!-- <hr> -->
    </body>
</html>
