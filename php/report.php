<?php
    session_start();
    if ($_SESSION['loggedIn']==false) {
        header('Location: /php/login.php'); 
    }

?>

<?php
    // function redirectTohttps() {
	//     if($_SERVER['HTTPS']!='on') {
	// 	    $redirect= 'https://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
    //         header("Location: $redirect"); 
	//     } 
    // }

    // redirectTohttps();

    $_SERVER["REMOTE_ADDR"] = "http://localhost:8080";
    date_default_timezone_set('America/New_York');
    require '../vendor/autoload.php';
    use Elasticsearch\ClientBuilder;
    $scheme = 'https';
    $hostname = 'elasticsearch-datastealth.apps.ocp-dev-ade.belldev.dev.bce.ca';
    $port = '443';
    // $username = 'logstash_internal';
    // $password = 'logstash_internal';
    $username = 'php_internal';
    $password = 'php_internal';

    $hosts = [
        [
            'host' => $hostname,
            'port' => $port,
            'scheme' => $scheme,
            'user' => $username,
            'pass' => $password
        ],
    ];
    
    // $myCert = '/etc/certs/BellIssuingCA4.cer';
    $client = ClientBuilder::create()           // Instantiate a new ClientBuilder
                        // ->setConnectionPool('\Elasticsearch\ConnectionPool\StaticNoPingConnectionPool', [])
                        ->setHosts($hosts)      // Set the hosts
                        // ->setSSLVerification($myCert)
                        ->setSSLVerification(false)
                        ->build();
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        <link rel="stylesheet" href="../styles/styles.css">
        <script src="../javascript/init.js"></script> 

        <title>Self Serve - Reporting Page</title>

        <div class="header" style="line-height: 0.3">
            <h1>
                <!-- <img style="vertical-align:top;margin:-5px 0px" src="/images/Datex.png" alt="Datex" width=40 height=40></img> -->
                <img src="/images/Bell.png" alt="Bell" width=90 height=50></img> 
                Self Serve Reporting
            </h1>
            <!-- <hr> -->
        </div>
        
    </head>
    <body>
        <div class="divsvg" height="20" width="100%">
            <button style="border: none;background: none;padding-top: 6px;">
                <svg height="20" width="20">
                        <path  class="svg-path" onclick="openMenu()" d="M0 2h16v2H0V2zm0 5h16v2H0V7zm16 5H0v2h16v-2z" >
                </svg>
            </button>
            <button style="border: none;background: none;position: absolute;right: 25px;top:105px;" >
                <input type="image" alt="logout" class="svg-path" src="/images/logout.png" width=20px height=20px onClick="javascript:redirect('login.php');"/>
            </button>
        </div>
        <!-- <div>
            <input type="image" style="vertical-align:middle;margin:-5px 0px" src="/images/home_icon.png" width=25 height=25 onClick="javascript:redirect('login.php');"/>
        </div> -->
        <?php
            $params = [
                'index' => 'ocp_processed.i.ds.raw.db_discovery-*',
                'size' => 0,
                'body'  => [
                    'aggs' => [
                        'ApplicationName' => [
                            'terms' => [
                                'field' => 'Header.ApplicationName.keyword'
                            ]
                        ]
                    ]
                ]
            ];
            
            $results = $client->search($params);
            // echo '<pre>', print_r($results, true), '</pre>';
            
            $resultNames = array_map(function($item) {
                return $item['key'];
            }, $results['aggregations']['ApplicationName']['buckets']);
            
            // echo '<pre>', print_r($resultNames, true), '</pre>';
        ?> 
        <form id="report_form"
            action=""
            method="post">
            <br>
            <text style="padding-left: 20px;">Database Discovery Application Name:</text>
                <select name="host">
                    <option value="">Please select</option>
                    <?php foreach($resultNames as $resultName): ?>
                        <option value="<?php echo $resultName; ?>"><?php echo $resultName; ?></option>
                        <li><?php echo $resultName; ?></li>
                    <?php endforeach; ?>
                </select> <br><br>
            <text style="padding-left: 20px;color:#a81106;font-style: italic;">***PDF Report generation can take 2-3 minutes. By default, report generation times out if the report cannot be generated within two minutes. If you are generating reports that contain many complex visualizations or your machine is slow or under constant heavy load, it might take longer than two minutes to generate a report***</text><br><br>
            <text style="padding-left: 20px;"><input type="submit" id="generate" name="generate" value="Generate"> </text>
                <!-- <div id="loading"></div>  -->
            <br> <br>
            <hr>
            <!-- <button onClick="window.print()">Print this page</button><br> <br> -->
        </form>
        <p>
            <?php
                $host = '';
                $hostname = '';
                    if (isset($_POST['generate'])) {
                        // echo htmlspecialchars($_POST['searchterm'],ENT_QUOTES);
                        if (isset($_POST['host'])) {
                            $host = $_POST['host'];
                            if ($host!='') {
                                // printf('User name: %s',htmlspecialchars($name,ENT_QUOTES));
                                printf('<p><b><text style="padding-left: 20px;"/>Host Name:</b> %s </p>',$host);
                                printf('<script language="javascript">show();</script>');
                                $index_name='a.ds0001.processed.db_discovery';
                                $hostname=rawurlencode(rawurlencode($host));
                                // echo "<b>Encoded:</b> $hostname";
                                // echo "<br><br>";
                                // $host='ITSEC%2520Benchmark';
                                $jobParams = "%28browserTimezone%3AAmerica%2FNew_York%2Clayout%3A%28dimensions%3A%28height%3A1240%2Cwidth%3A1842%29%2Cid%3Apreserve_layout%29%2CobjectType%3Adashboard%2CrelativeUrls%3A%21%28%27%2Fapp%2Fdashboards%23%2Fview%2F1bc30640-5133-11eb-986c-312951d7e825%3F_g%3D%28filters%3A%21%21%28%29%2CrefreshInterval%3A%28pause%3A%21%21t%2Cvalue%3A0%29%2Ctime%3A%28from%3Anow-30d%2Cto%3Anow%29%29%26_a%3D%28description%3A%21%27%21%27%2Cfilters%3A%21%21%28%28%21%27%24state%21%27%3A%28store%3AappState%29%2Cmeta%3A%28alias%3A%21%21n%2CcontrolledBy%3A%21%271610057398094%21%27%2Cdisabled%3A%21%21f%2Cindex%3A$index_name%2Ckey%3AHeader.ApplicationName.keyword%2Cnegate%3A%21%21f%2Cparams%3A%28query%3A%21%27$hostname%21%27%29%2Ctype%3Aphrase%29%2Cquery%3A%28match_phrase%3A%28Header.ApplicationName.keyword%3A%21%27$hostname%21%27%29%29%29%29%2CfullScreenMode%3A%21%21f%2Coptions%3A%28hidePanelTitles%3A%21%21f%2CuseMargins%3A%21%21t%29%2Cquery%3A%28language%3Akuery%2Cquery%3A%21%27%21%27%29%2CtimeRestore%3A%21%21t%2Ctitle%3A%21%27d.ds.database_discovery_dashboard%2520-%2520by%2520Aplication%21%27%2CviewMode%3Aview%29%27%29%2Ctitle%3A%27d.ds.database_discovery_dashboard%20-%20by%20Aplication%27%29";
                                // $jobParams = "%28browserTimezone%3AAmerica%2FNew_York%2Clayout%3A%28dimensions%3A%28height%3A2864%2Cwidth%3A1842%29%2Cid%3Apreserve_layout%29%2CobjectType%3Adashboard%2CrelativeUrls%3A%21%28%27%2Fapp%2Fdashboards%23%2Fview%2F4a6532e0-2ab7-11eb-870b-63703208df3c%3F_g%3D%28filters%3A%21%21%28%29%2CrefreshInterval%3A%28pause%3A%21%21t%2Cvalue%3A0%29%2Ctime%3A%28from%3Anow-30d%2Cto%3Anow%29%29%26_a%3D%28description%3A%21%27%21%27%2Cfilters%3A%21%21%28%28%21%27%24state%21%27%3A%28store%3AappState%29%2Cmeta%3A%28alias%3A%21%21n%2Cdisabled%3A%21%21f%2Cindex%3A$index_name%2Ckey%3AHeader.HostName.keyword%2Cnegate%3A%21%21f%2Cparams%3A%28query%3A$host%29%2Ctype%3Aphrase%29%2Cquery%3A%28match_phrase%3A%28Header.HostName.keyword%3A$host%29%29%29%29%2CfullScreenMode%3A%21%21f%2Coptions%3A%28hidePanelTitles%3A%21%21f%2CuseMargins%3A%21%21t%29%2Cquery%3A%28language%3Akuery%2Cquery%3A%21%27%21%27%29%2CtimeRestore%3A%21%21t%2Ctitle%3A%21%27d.ds.database_discovery_dashboard%2520-%2520Generic%21%27%2CviewMode%3Aview%29%27%29%2Ctitle%3A%27d.ds.database_discovery_dashboard%20-%20Generic%27%29";
                                $url = "https://kibana-datastealth.apps.ocp-dev-ade.belldev.dev.bce.ca/api/reporting/generate/printablePdf?jobParams=$jobParams";
                                

                                $ch = curl_init();
                                // echo $jobParams;
                                // echo "URL found";
                                curl_setopt($ch, CURLOPT_URL, $url);
                                curl_setopt($ch, CURLOPT_POST, 1);
                                curl_setopt($ch, CURLOPT_USERPWD, $username . ':' . $password);
                                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                
                                $headers = [
                                    'kbn-xsrf: reporting',
                                    'kbn-version: 7.9.1',
                                    'Accept:application/json; charset=utf-8',
                                    'Content-type:application/json; charset=utf-8'
                                ];
                
                                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        
                                try {
                                    $output = curl_exec($ch);
                                    $result =json_encode($output, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
                                    $data = json_decode($output);
                                    $res_url = "https://kibana-datastealth.apps.ocp-dev-ade.belldev.dev.bce.ca".$data->path;
                                    // print_r("<hr><b>Download Path:</b> <pre>$res_url</pre>"); 
                                    curl_close ($ch); 
                                    // print_r("<hr><text style='padding-left: 20px;'/><b>POST Result:</b> <text style='padding-left: 20px;'/><pre>$result</pre>"); 
                                    // print_r("<pre>".$result."</pre><hr>");
                                } catch (Exception $e) {
                                    curl_close ($ch); 
                                    echo 'Caught exception: ',  $e->getMessage(), "\n";
                                };
                
                                // sleep(30);
                                // echo "Sleep over";
                                $ch = curl_init();
                                
                                curl_setopt($ch, CURLOPT_URL, $res_url);
                                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET"); 
                                curl_setopt($ch, CURLOPT_FILE, $GlobalFileHandle);
                                curl_setopt($ch, CURLOPT_USERPWD, $username . ':' . $password);
                                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                
                                $body = '{}';
                                // $attachment_location = "../downloads/test.pdf";
                                $headers = [
                                    'kbn-version: 7.9.1',
                                    'Accept:application/pdf',
                                    'Content-type: application/pdf',
                                    // 'Content-Transfer-Encoding: Binary',
                                    'Content-Type: application/force-download'
                                    // 'Content-Length:'.filesize($attachment_location),
                                    // 'Content-Disposition: inline; filename='.$attachment_location
                                ];
                
                                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                                curl_setopt($ch, CURLOPT_HEADER, 0);
                                curl_setopt($ch, CURLOPT_POSTFIELDS,$body);
        
                                try {
                                    $key = true;
                                    $starttime = new DateTime(date('H:i:s'));
                                    // printf('<script language="javascript">show_pdf_load();</script>');
                                    while ($key) {
                                        $res = curl_exec($ch);  
                                        if (preg_match("/PDF/",$res)) {
                                            $key = false;
                                            $endtime = new DateTime(date('H:i:s'));
                                            // printf('<script language="javascript">hide_pdf_load();</script>');
                                            break;
                                        } 
                                    }
                                    $diffe = $endtime->diff( $starttime );
                                    
                                    // print_r("<text style='padding-left: 20px;'/><b>Elapsed Time:</b> <text style='padding-left: 20px;'/> "); 
                                    printf('<p><b><text style="padding-left: 20px;"/>Elapsed Time <i>(hh:mm:ss)</i>:</b> %s </p>',$diffe->format( '%H:%I:%S' ));
                                    // echo $diffe->format( '%H:%I:%S' );
                                    // print_r("<br>"); 
        
                                    $filesize = curl_getinfo($ch, CURLINFO_CONTENT_LENGTH_DOWNLOAD);
                                    if (curl_errno($ch)) {
                                        echo 'Error:' . curl_error($ch);
                                    }
                                    curl_close($ch);
                                    // print_r("Filesize: <pre>".$filesize."</pre><hr>"); 
                                    // Inject $res into a file //
                                    // POP up the file for user to download //
                                    
                                    $filename= 'datastealth_report_'.date('Y-m-d_H_i_s').'.pdf';
                                    $directory="downloads";
                                    $file=fopen("../$directory/$filename", "w") or die("Unable to open file!");
                                    $urlfile=$_SERVER["REMOTE_ADDR"]."/$directory/$filename";
                                    // echo $urlfile;
                                    fwrite($file, $res);
                                    // file_put_contents($file, $res);
                                    fclose($file);
                                    print_r("<text style='padding-left: 20px;'/><b>Downloaded Report:</b> <br><br>"); 
                                    print_r("<iframe src='$urlfile' frameBorder='0' scrolling='auto' height='900px' width='100%'></iframe>");
                                    
                                    // unlink("../$directory/$filename");
                                    // print_r("Result: <br><pre>".$res."</pre><hr>");  
                                } catch (Exception $e) {
                                    curl_close($ch);
                                    echo 'Caught exception: ',  $e->getMessage(), "\n";
                                };
                                // printf('<iframe style="border: none;" src="https://kibana-datastealth.apps.ocp-dev-ade.belldev.dev.bce.ca/app/visualize#/edit/86ef8210-2f4e-11eb-870b-63703208df3c?embed=true&_g=(filters:!(),refreshInterval:(pause:!t,value:0),time:(from:now-30d,to:now))&_a=(query:(language:lucene,query:\'Header.HostName.keyword:%s\'))" height="400" width="700"></iframe>',$host);
                                // printf('<iframe style="border: none;" src="https://kibana-datastealth.apps.ocp-dev-ade.belldev.dev.bce.ca/app/visualize#/edit/006f37b0-2f50-11eb-870b-63703208df3c?embed=true&_g=(filters:!(),refreshInterval:(pause:!t,value:0),time:(from:now-30d,to:now))&_a=(query:(language:lucene,query:\'Header.HostName.keyword:%s\'))" height="400" width="700"></iframe>',$host);
                                // $_SESSION['loggedIn']=false;
                            };
                        }
                        //  else {
                        //     printf('<script language="javascript">unhide();</script>');
                        //  };
                    }
            ?>
        </p>
    </body>
</html>