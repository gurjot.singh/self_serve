<?php
    exec("sudo /usr/bin/php /var/www/html/php/pdf_report.php");
    require 'vendor/autoload.php';
    use Elasticsearch\ClientBuilder;
    $scheme = 'https';
    $hostname = 'elasticsearch-datastealth.apps.ocp-dev-ade.belldev.dev.bce.ca';
    $port = '443';
    $username = 'datastealth_internal';
    $password = 'dAtA5t3a1th@2020';
    $db_index = 'ocp_processed.i.ds.raw.db_discovery-*';
    $file_index = 'ocp_processed.i.ds.raw.file_discovery-*';

    $hosts = [
        [
            'host' => $hostname,
            'port' => $port,
            'scheme' => $scheme,
            'user' => $username,
            'pass' => $password
        ],
    ];
    
    $client = ClientBuilder::create()           
                        // ->setConnectionPool('\Elasticsearch\ConnectionPool\StaticNoPingConnectionPool', [])
                        ->setHosts($hosts)      
                        // ->setSSLVerification($myCert)
                        ->setSSLVerification(false)
                        ->build();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="/styles/styles.css">
    <script src="/javascript/init.js"></script> 

    <title>ElasticSearch Engine</title>

    <div class="header">
        <h1>
            <!-- <img style="vertical-align:top;margin:-5px 0px" src="/images/Datex.png" alt="Datex" width=40 height=40></img> -->
            <img src="/images/Bell.png" alt="Bell" width=90 height=50></img> 
             ElasticSearch Engine
        </h1>
        <!-- <hr> -->
    </div>
</head>
<body>
    <!-- <h1>Elasticsearch Nodes:</h1>
    <?php
        $params_node = [
        ];
        $response_node = $client->cat()->nodes($params_node);
        echo '<pre>', print_r($response_node, true), '</pre>';
    ?>
    <h1>Elasticsearch Indices:</h1>
    <?php
        $params_index = [
        ];
        
        $response_index = $client->cat()->indices($params_index);
        echo '<pre>', print_r($response_index, true), '</pre>';
    ?> -->
    <!-- <h1>DataStealth Elasticsearch Results:</h1> -->
    <div id="elastic_info" class="elastic_info">
        <ul>
            <li><b>Scheme:</b> <i><?php echo $scheme; ?></i></li>
            <li><b>HostName:</b> <i><?php echo $hostname; ?></i></li>
            <li><b>Username:</b> <i><?php echo $username; ?></i></li>
        </ul>
    </div> <br>
    <hr>
    <?php
        $params_db = [
            'index' => $db_index,
            'size' => 0,
            'body'  => [
                'aggs' => [
                    'HostName' => [
                        'terms' => [
                            'field' => 'Header.HostName.keyword'
                        ]
                    ]
                ]
            ]
        ];
        
        $results_db = $client->search($params_db);
        $resultNames_db = array_map(function($item_db) {
            return $item_db['key'];
        }, $results_db['aggregations']['HostName']['buckets']);
        
        // echo '<pre>', print_r($resultNames, true), '</pre>';

        $params_file = [
            'index' => $file_index,
            'size' => 0,
            'body'  => [
                'aggs' => [
                    'HostName' => [
                        'terms' => [
                            'field' => 'Header.HostName.keyword'
                        ]
                    ]
                ]
            ]
        ];
        
        $results_file = $client->search($params_file);
        $resultNames_file = array_map(function($item_file) {
            return $item_file['key'];
        }, $results_file['aggregations']['HostName']['buckets']);
        
        // echo '<pre>', print_r($resultNames, true), '</pre>';
    ?> 
    <form id="es_form"
            action=""
            method="post">
            <br>
            <table>
                <tr>
                    <td><p style="padding: 5px;">Database Discovery Host name: </p></td>
                    <td><select name="host_db">
                            <option disable value="">Values......</option>
                            <?php foreach($resultNames_db as $resultName): ?>
                                <option value="<?php echo $resultName; ?>"><?php echo $resultName; ?></option>
                                <li><?php echo $resultName; ?></li>
                            <?php endforeach; ?>
                        </select></td>
                    <td><p style="padding: 5px;">File Discovery Host name: </p></td>
                    <td><select name="host_file">
                            <option disable value="">Values......</option>
                            <?php foreach($resultNames_file as $resultName): ?>
                                <option value="<?php echo $resultName; ?>"><?php echo $resultName; ?></option>
                                <li><?php echo $resultName; ?></li>
                            <?php endforeach; ?>
                        </select></td>
                    <td style="padding: 5px;"><input type="submit" id="submit" name="submit" value="Submit"></td>
                <tr>
            </table>
            <hr>
            <!-- <button onClick="window.print()">Print this page</button><br> <br> -->
    </form>
    <p>
            <?php
                $host_db = '';
                $host_file = '';
                    if (isset($_POST['submit'])) {
                        // echo htmlspecialchars($_POST['searchterm'],ENT_QUOTES);
                        if (isset($_POST['host_db'])) {
                            $host_db = $_POST['host_db'];
                            if ($host_db!='') {
                                // printf('User name: %s',htmlspecialchars($name,ENT_QUOTES));
                                printf('<p><b>Database Discovery Host name <i>(10 Results only)</i>:</b> %s </p><hr>',$host_db);
                                $params_db_search = [
                                    'index' => $db_index,
                                    'size' => 10,
                                    'body'  => [
                                        'query' => [
                                            'match' => [
                                                'Header.HostName.keyword' => $host_db
                                            ]
                                        ]
                                    ]
                                ];
                                
                                $results_db_search = $client->search($params_db_search);
                                $resultNames_db_search = array_map(function($item_db_search) {
                                    return $item_db_search['_source'];
                                }, $results_db_search['hits']['hits']);

                                echo "<pre>".json_encode($resultNames_db_search, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES)."</pre><hr>";
                            };
                        }

                        if (isset($_POST['host_file'])) {
                            $host_file = $_POST['host_file'];
                            if ($host_file!='') {
                                // printf('User name: %s',htmlspecialchars($name,ENT_QUOTES));
                                printf('<p><b>File Discovery Host name <i>(10 Results only)</i>:</b> %s </p><hr>',$host_file);
                                $params_file_search = [
                                    'index' => $file_index,
                                    'size' => 10,
                                    'body'  => [
                                        'query' => [
                                            'match' => [
                                                'Header.HostName.keyword' => $host_file
                                            ]
                                        ]
                                    ]
                                ];
                                
                                $results_file_search = $client->search($params_file_search);
                                $resultNames_file_search = array_map(function($item_file_search) {
                                    return $item_file_search['_source'];
                                }, $results_file_search['hits']['hits']);

                                echo "<pre>".json_encode($resultNames_file_search, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES)."</pre><hr>";
                            };
                        }
                    }
            ?>
        </p>
</body>
</html>
